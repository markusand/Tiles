# Tiles

Tiles is a surface rendering grid, similar to first versions of SimCity, based on a physical Lego grid. Inspiration comes from [Ira Winder](https://github.com/irawinder)'s work in Colortizer / Legotizer and CityScope. 

## Setup

Create a tsv file containing color settings

	ID	NAME	VALUE
	0	WHITE	rgba(255,255,255,255)
	1	BLACK	rgba(0,0,0,255)
	2	RED		#FFFF0000

where:
- **ID** is a unique identifier
- **VALUE** can be an #aarrggbb or rgba() color

Create a tsv file containing tiles' settings

	ID	TYPE	TL	TR	BL	BR	ASPECT	RENDER
	0	Empty	0	0	0	0	COLOR	#44000000	
	1	Land	2	0	0	1	COLOR	rgba(0,255,0,255)
	2	Street	2	0	1	0	TEXTURE	street.png
	3	Spare	2	0	1	1	CODE
	...
	7	xxxx	X	1	1	1	xxxx	xxxx

where:
+ **ID** and **TYPE** are unique identifiers
+ **TL** = TopLeft, **TR** = TopRight, **BL** = BottomLeft and **BR** = BottomRight is the color code (using color ID)
+ **ASPECT** and **RENDER** determine the rendering method (*CODE*, *COLOR*, *TEXTURE*)
  + *CODE* draws the color code. No need for *RENDER* parameter, it is ignored
  + *COLOR* applies the specified color in *RENDER*, that can be an #aarrggbb or rgba() color
  + *TEXTURE* applies the specified texture from a file in data/ directory


## Usage
	
Create a Tiles grid, specifying the settings files beforehand created. If no settings files are given, it will retrieve "colors.tsv" and "tiles.tsv" as default
Create a Scanner that will read the lego surface and generate the grid layout message to be rendered

	Tiles tableau = new Tiles("colors.tsv", "tiles.tsv");
	Scanner scanner = new Scanner(this);
	

Run the scanner. Once running is possible to parametrize grid layout and color detection references

    scanner.run();


Load input data whenever needed/desired (p.e. periodically or after a specific key press)
    
    if( scanner.available() ) {
        String msg = scanner.read();
        tableau.update(msg);
    }
    

Draw Tiles at given location and size. Tile surface can be drawn in 2D or 3D

    if( scanner.isScanning() ) {
	    tableau.set3D(false);
        tableau.draw(50, 80, 10);
        tableau.set3D(true);
        tableau.draw(width/2, height/2, 80);
	}


# Message format

*msg* is a string containing the grid information with the format

	5	5
	3	0	0	0
	5	1	0	90
	5	2	0	0
	2	3	0	180
	6	4	0	90
	1	0	1	0

where first line is the grid size (Columns & Rows) and following lines represent a block each one. First value is the type *ID* of the block, while next two values are its (x,y) position in the grid and the last value is the block rotation in degrees.


## License

Tiles is released under the MIT License.