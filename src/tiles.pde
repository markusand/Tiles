/*
 * TILES
 * 
 * Generate a Tiles (textured grid) surface, where every parameter is defined by a streaming message
 * that contains the loyout and every tile configuration
 *
 * @author          Marc Vilella
 *                  Observatori de la Sostenibilitat d'Andorra (OBSA)
 *                  mvilella@obsa.ad
 * @contributors    
 * @copyright       Copyright (c) 2016 Marc Vilella
 * @license         MIT License
 * @required   
 * @version         1.0
 *
 * @bugs       
 *
 * @todo            Identify neighbors and adapt texture to them
 *                  3D object tiles
*/



// TILES CLASS =========================================================================================================================
public class Tiles {

    /* ATTRIBUTES ---------------------------------------------------------------------------------> */

    private static final String CODE_FILE = "tiles.tsv";
    private static final String COLOR_FILE = "colors.tsv";

    private ArrayList<Tile> grid;
    private int[] size;  // Rows & Cols
    private float rotation = 0;
    private String buffer = "";

    IntList colors;
    HashMap<Integer, Tile> molds;
  
    private boolean mousePrevState,
                    mouseClicked,
                    mouseReleased,
                    mouseDragged;
    
    
    private boolean view3D = false;
  
  
  
    /* CONSTRUCTORS ------------------------------------------------------------------------------> */
    
    /*
    * Construct a Tiles surface with default settings
    */
    Tiles(PApplet parent) {
        this(parent, COLOR_FILE, CODE_FILE);
    }
  
    /*
    * Construct a Tiles surface from provided files
    * @param colorSettings  File path with colors' settings
    * @param tileSettings  File path with tiles' settings
    */
    Tiles(PApplet parent, String colorSettings, String tileSettings) {
        parent.registerMethod("mouseEvent", this);
        grid = new ArrayList<Tile>();
        loadSettings(colorSettings, tileSettings);
        clear();
    }
  
  
    /* GETTERS & SETTERS --------------------------------------------------------------------------> */
    
    public void set3D(boolean view3D) { this.view3D = view3D; }
  
  
    /* METHODS ------------------------------------------------------------------------------------> */
    
    /*
    * Clear surface to empty state
    */
    public void clear() {
        grid = new ArrayList<Tile>();
        size = new int[2];
    }
  
  
  
    /*
    * Update surface tiles
    * @param msg  Surface layout and tiles' configuration
    */
    public void update(String msg) {
        if(!buffer.equals(msg)) {  // Something has changed
            this.clear();  // Reset grid
            String[] lines = split(trim(msg), "\n");
            size = int(split(lines[0], "  "));  // Grid cols & rows [At line 0]
            // Blocks reading (start at line 1) --->
            for (int i = 1 ; i < lines.length; i++) {
                String[] blockLine = split(lines[i], "  ");  // [0] typeID  [1] xPos  [2] yPos  [3] Rotation
                createTile(int(blockLine[0]), int(blockLine[1]), int(blockLine[2]), int(blockLine[3]));
            }
            buffer = msg;
        }
    }
  
  
  
    /*
    * Create new tile cloning from a mold and adding location attributes
    * @param typeID  Type ID of the tile to be created
    * @param x  Column position in the surface
    * @param y  Row position in the surface
    * @param rotation  Rotation of the tile (0, 90, 180, 270)
    */
    public void createTile(int typeID, int x, int y , int rotation ){
        Tile tile = molds.get(typeID).clone();
             tile.setPosition(x, y);
             tile.setRotation(rotation);
        grid.add( tile );
    }
  
  
  
    /*
    * Draw Tiles surface
    * @param x  x-coordinate of the surface's center
    * @param y  y-coordinate of the surface's center
    * @param tileSize tile side size
    */
    public void draw(int x, int y, int tileSize) {
        pushMatrix();
            translate(x, y);
            if(view3D) {
                rotateX(PI/3);
                rotateZ(rotation);
            }
            translate( -(tileSize * size[0]) / 2, -(tileSize * size[1]) / 2);
            for(Tile tile : new ArrayList<Tile>(grid)) tile.draw( tileSize );
        popMatrix();
    }

  
  
    /*
    * Update rotation angle for 3D view
    * @param angle  Rotation angle in the z-axis
    */
    public void rotate( float angle ) {
        if(view3D) rotation += angle;
    }
  
  
  
    /*
    * Load settings from files
    * @param colorSettings  File path containing colors definitions
    * @param tileSettings  File path containing tiles definitions
    */
    public void loadSettings(String colorSettings, String tileSettings) {
        if(colorSettings != null) colors = loadColorSettings(colorSettings);
        if(tileSettings != null) molds = loadTileSettings(tileSettings);
    }
  
  
  
    /*
    * Load colors settings
    * @param colorsPath File path containing all definitions
    * @return list of color object referenced by its id, or null if file doesn't exist
    */
    private IntList loadColorSettings(String colorsPath) {
        File colorsFile = new File(dataPath(colorsPath));
        if(colorsFile.exists()) {
            IntList colors = new IntList();
            Table cS = loadTable(colorsPath, "header, tsv");
            for(TableRow c : cS.rows()) {
                colors.append(decodeColor(c.getString("VALUE")));
            }
            return colors;
        } else return null;
    }
  
  
  
    /*
    * Load Tiles parameters from file and create sample tiles to be cloned in the future
    * @param    settingsPath    File path containing all Tiles parameters
    * @return   HashMap<Int, Tile> containing a sample of every different tile referenced by
    *           its id, or null if file doesn't exist
    */
    private HashMap<Integer, Tile> loadTileSettings(String settingsPath) {
        File settingsFile = new File(dataPath(settingsPath));
        if(settingsFile.exists()) {
            HashMap<Integer, Tile> molds = new HashMap<Integer, Tile>();
            Table tiles = loadTable(settingsPath, "header,tsv");
            for(TableRow mold : tiles.rows()) {
                int[] moldCode = new int[] { mold.getInt("TL"), mold.getInt("TR"), mold.getInt("BL"), mold.getInt("BR") };
                String aspect = mold.getString("ASPECT");
                PShape tile = null;
                // Tile is displayed as single color --->
                if(aspect.equals("COLOR")) {
                    color fill = decodeColor(mold.getString("RENDER"));
                    tile = createShape(RECT, 0, 0, 1, 1);
                    tile.setFill(fill);
                // Tile is displayed with a texture image --->
                } else if( aspect.equals("TEXTURE") ) {
                    PImage texture = loadImage("textures/" + mold.getString("RENDER"));
                    tile = createShape(RECT, 0, 0, 1, 1);
                    tile.setTexture(texture);
                // Displays tile's code --->
                } else {
                    tile = createShape(GROUP);
                    int cols = floor(sqrt(moldCode.length));
                    for(int i = 0; i < moldCode.length; i++) {
                        PShape qC = createShape(RECT, (i % cols), (i / cols), 1, 1);
                        qC.setFill( colors.get( moldCode[i] ) );
                        tile.addChild(qC);
                    }
                }
                tile.setStroke(false);
                molds.put( mold.getInt("ID"), new Tile(mold.getInt("ID"), mold.getString("TYPE"), moldCode, tile));
            }
            return molds;
        } else return null;
    }
  
  
  
    /*
    * Decode color written in 8-bit hexadecimal format { #AARRGGBB } or rgba { rgba(RR,GG,BB,AA) }
    * @param c  String definition of the color
    * @return int color object, black if color cannot be decoded
    */
    private color decodeColor(String c) {
        // Hex format #AARRGGBB
        if( c.indexOf("#") != -1 ) {  
            return unhex( c.substring(1) );
        // aRGB format rgba(RR, GG, BB, AA)
        } else if( c.indexOf("rgba(") != -1 ) {  
            String[] RGBA = split( c.substring(5, c.length()-1) , ",");
            return color( int(RGBA[0]), int(RGBA[1]), int(RGBA[2]), int(RGBA[3]) ); 
        // Use dictionary for color
        } else {
        }
        return 0;
    }
    
    
    public void mouseEvent(MouseEvent event) {
        switch(event.getAction()) {
            case MouseEvent.PRESS:
                break;
            case MouseEvent.DRAG:
                rotate( map(mouseX-pmouseX, 0, width, 0, PI) );
                break;
            case MouseEvent.RELEASE:
                break;
        }
        
    }
    
}


// TILE CLASS =========================================================================================================================
public class Tile {
  
    /* ATTRIBUTES ----------------------------------------------------------------------------------> */
    
    private int typeID;
    private String name;
    private int[] position;
    private int[] code;    // Consider to remove
    private int rotation;
    private PShape shape;
  
  
  
    /* CONSTRUCTORS --------------------------------------------------------------------------------> */
      
    /*
    * Construct a Tile with its basic parameters: codification and texture
    * @params typeID  Unique ID for its type
    * @params name  Name of the type Tile
    * @params code  4 colors codificated code to identify it
    * @params shape Visual shape and texture
    */
    Tile(int typeID, String name, int[] code, PShape shape) {
        this.typeID = typeID;
        this.name = name;
        this.code = code;
        this.shape = shape;
    }
  
  
  
    /* GETTERS & SETTERS ---------------------------------------------------------------------------> */
  
    public void setRotation(int r) { rotation = r; }
    public void setPosition(int x, int y) { position = new int[] {x, y}; }
  
  
  
    /* METHODS -------------------------------------------------------------------------------------> */
    
    /*
    * Return a clone of itself
    * @return new tile that's a copy of this
    */
    public Tile clone() {
        return new Tile(typeID, name, code, shape);
    }
  
  
  
    /*
    * Draw tile's visual shape
    * @param size  Side size of the tile to be drawn
    */
    public void draw(int size) {
        pushMatrix();
            translate( (position[0] + 0.5) * size, (position[1] + 0.5) * size);
            rotate( radians(rotation) );
            translate(-size/2, -size/2);
            shape(shape, 0, 0, size, size);    
        popMatrix();
    }
  
    
    
    /*
    * Checks if this tile's code matches to a specific one
    * @param code  Color code to look for
    * @return boolean true if this tile has the code, false if not
    */
    public boolean hasCode(int[] code) {
        for(int i = 0; i < this.code.length; i++) {
            if( this.code[i] != code[i]) return false;
        }
        return true;
    }
  
}