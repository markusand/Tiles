/* <--- LIBRARIES ---> */
import hypermedia.net.*;  // UDP

Scanner scanner;
Tiles tableau;
UDP udp;

void setup() {
    
    size(960, 540, P3D);
    
    //udp = new UDP( this, 6000 );
    udp = new UDP(this);
    udp.listen(true);
    
    scanner = new Scanner(this);
    scanner.run();
    
    tableau = new Tiles(this, "colors.tsv", "tiles.tsv");
  
}

void draw() {
    
    background(#555555);
    
    scanner.run();
    
    if( scanner.isScanning() ) {
        if( scanner.available() ) {
            String msg = scanner.read();
            String ip       = "localhost";   // the remote IP address of Host
            int port        = 6000;     // the destination port
            udp.send( msg, ip, port );
            tableau.update(msg);
        }
        
        tableau.set3D(false);
        tableau.draw(50, 80, 10);
        tableau.set3D(true);
        tableau.draw(width/2, height/2, 80);
    }

}

/*
void receive(byte[] data) {
    String msg = new String(data);
    tableau.update(msg);
}
*/