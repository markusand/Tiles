/*
 * LEGO GRID SCANNER
 * 
 * Scan a grid of codified Lego tiles and create a message with the configuration
 * to be read by other functions
 *
 * @author          Marc Vilella
 *                  Observatori de la Sostenibilitat d'Andorra (OBSA)
 *                  mvilella@obsa.ad
 * @contributors    
 * @copyright       Copyright (c) 2016 Marc Vilella
 * @license         MIT License
 * @required   
 * @version         1.0
 *
 * @bugs       
 *
 * @todo            Complete UDP sending messages
 *                  Tiles rotation in message
 *                  Unwarp distorted grid method
*/


/* ATTRIBUTES ---------------------------------------------------------------------------------> */

import java.util.Map;
import processing.video.*;


/* ENUM DEFINITIONS ---------------------------------------------------------------------------------> */

public enum Mode { CONFIG, CONFIG_COLOR, SCAN; }


// SCANNER CLASS =========================================================================================================================
public class Scanner {
  
    /* ATTRIBUTES ---------------------------------------------------------------------------------> */
  
    private static final String CONFIG_FILE = "scanner.txt";
    private static final String CODE_FILE = "tiles.tsv";
    private static final String COLOR_FILE = "colors.tsv";
      
    private Mode mode;
    private Capture cam;
    private ArrayList<Grid> grids;
      
    private final float ASPECT_RATIO = 4f/3f;
      
    private HashMap<Integer, int[]> codeRef;
    private IntList legoColors;
    private IntList refColors;
    private int iColor;
      
    private String msg;
    private boolean messageReady = false;
    
    // Default parameters  
    int[] camSize = new int[] { 640, 480 };
    int fRate = 2;
  
  
  
    /* CONSTRUCTORS -------------------------------------------------------------------------------> */
    
    /*
    * Construct a scanner with default settings
    */
    public Scanner(PApplet parent) {
        this(parent, CONFIG_FILE, CODE_FILE, COLOR_FILE);
    }
    
    
    /*
    * Construct a scanner from provided files
    * @params configPath File path with cam and grid settings
    * @params colorsPath  File path with colors' settings
    * @params codesPath  File path with codes' settings
    */
    public Scanner(PApplet parent, String configPath, String colorsPath, String codesPath) {
        
        println("LOADING SCANNER...");
        
        parent.registerMethod("keyEvent", this);
        parent.registerMethod("mouseEvent", this);
        
        grids = new ArrayList<Grid>();
    
        // Read config file if any, or create new one
        File configFile = new File( dataPath(CONFIG_FILE) );
        if(!configFile.exists()) {
            println("Config file doesn't exist...");
            saveCamSettings();
            println(" File created.");
            mode = Mode.CONFIG;
          
        // Confiuration file exists. Read it!
        } else {
            print("Reading config file...");
            loadCamSettings(configFile);
            println("DONE");
            mode = Mode.SCAN;
          
        }
    
        // Init camera
        cam = new Capture(parent, camSize[0], camSize[1], fRate);
        // 20 960x640px 1fps
        // 23 480x270px 1fps
        // 26 240x135px 1fps
        //String[] cameras = Capture.list();
        //cam = new Capture(parent, cameras[20]);
        cam.start();
    
        // Load CODEs & COLORs settings
        loadTableSettings(CODE_FILE, COLOR_FILE);
        refColors = new IntList( legoColors.array() );
        
        println("SCANNER READY TO RUN.");
    
    }
  
  
  
    /* METHODS ---------------------------------------------------------------------------------> */
  
    /*
    * Returns if scanner is in scanning mode or otherwise is in configuration mode
    * @return boolean true if in scanning mode, false if any other state
    */
    public boolean isScanning() {
        return mode == Mode.SCAN;
    }
  
  
  
    /*
    * Returns when scanner has a new message available to be read
    * @return boolean true if new message is ready
    */
    public boolean available() {  // Data available only if grid changed
        return messageReady;
    }
  
  
  
    /*
    * Return new message that is ready. Cleans the message ready flag
    * to avoid multiple lectures of the same message
    * @return the message
    */
    public String read() {
        messageReady = false;
        return msg;
    }
  
  
  
    /*
    * Create new grid with specified configuration
    * @param x1  x-coordinate of Top-Left grid corner
    * @param y1  y-coordinate of Top-Left grid corner
    * @param x2  x-coordinate of Bottom-Right grid corner
    * @param y2  y-coordinate of Bottom-Right grid corner
    * @param cols  Grid columns
    * @param rows  Grid rows
    */
    private void createGrid(int x1, int y1, int x2, int y2, int cols, int rows) {
        grids.add( new Grid(grids.size(), x1, y1, x2, y2, cols, rows) );
    }
  
  
  
    /*
    * Run the scanner. Different modes are provided: CONFIG to manage grids, CONFIG_COLOR to change
    * colors reference, and SCAN to evaluate all tiles and prepare an update message (if required)
    */
    public void run() {
        switch(mode) {
            case CONFIG:
                config();
                break;
            case CONFIG_COLOR:
                changeColorReference();
                break;
            case SCAN:
                scan();
                break;
        }
    }
  
  
    /*
    * Scanner CONFIG mode. Draw and manage grids
    */
    private void config() {
    
        PImage mirrorFrame = new PImage();
        if(cam.available() == true) {
            cam.read();
            mirrorFrame = flipImage(cam);
        }
        set(0, 0, mirrorFrame);
        
        // Grids drawing
        for(Grid grid : grids) {
            grid.eval(mirrorFrame, refColors);
            grid.draw(refColors);
        }
        
        // CONFIG instructions
        String instr = "CONFIGURATION MODE";
               instr += "\n[R] Change color REFERENCE";
               instr += "\n[+] ADD grid";
               instr += "\n[-] DELETE selected grid";
               instr += "\n[UP][DOWN] Change ROWS";
               instr += "\n[LEFT][RIGHT] Change COLUMNS";
               instr += "\n[S] SAVE & Exit";
        fill(255); textSize(9); textAlign(LEFT, TOP);
        text(instr, 10, 10);
        
        
    }
  
  
  
    /*
    * Scanner COLOR CONFIG mode. Select new colors as reference
    * for scanning color detection
    */
    private void changeColorReference() {
        
        noCursor();
        PImage mirrorFrame = new PImage();
    
        if (cam.available() == true) {
            cam.read();
            mirrorFrame = flipImage(cam);
        }
        set(0, 0, mirrorFrame);
        
        // Pointer color selector
        PImage underMouse = get(mouseX - 5, mouseY - 5, 10, 10);
        fill( getAvgColor(underMouse) ); stroke(#FFFFFF); strokeWeight(1);
        rect(mouseX - 10, mouseY - 10, 10, 10);
        
        // Instructions
        String instr = "COLOR REFERENCE ADJUSTMENT";
               instr += "\n[Mouse click] SELECT color";
               instr += "\n[ENTER] SAVE color";
               instr += "\n[S] SAVE & Exit";
        fill(255); textSize(9); textAlign(LEFT, TOP);
        text(instr, 10, 10);
        
        // Colors menu
        rectMode(CENTER);
        for(int i = 0; i < refColors.size(); i++) {
            int posY = 85 + i * 25;
            fill( refColors.get(i) ); stroke( legoColors.get(i) ); strokeWeight(2);
            if(i == iColor) {
                rect(25, posY, 15, 15);
                fill(#FFFFFF); noStroke(); textAlign(LEFT, CENTER);
                text("SELECT COLOR", 45, posY);
            } else rect(25, posY, 10, 10);
        }
        rectMode(CORNER);
        
    }
    
  
  
    /*
    * Scanner SCAN mode. Detect all colors in grid and prepare a message with
    * the table layout. Scanning is performed every time a new frame is
    * available (defined by cam framerate)
    */
    private void scan() {
    
        PImage mirrorFrame = new PImage();
    
        // Scan ONLY when new frame is available (defined by webcam framerate)
        if(cam.available() == true) {
            cam.read();
            mirrorFrame = flipImage(cam);
          
            // Scan and evaluate all grid's tiles.
            // If something changes gather state and prepare to send
            boolean msgSend = false;
            StringList msgLines = new StringList();
          
            for(Grid grid : grids) {
                grid.eval(mirrorFrame, refColors);
                if(grid.hasChanged()) msgSend = true;
                msgLines.append( grid.getMsg(codeRef) );
            }
            if(msgSend) {
                msg = join(msgLines.array(), "\n");
                messageReady = true;
            }
        }
    
        // Instructions
        String instr = "RUN MODE";
               instr += "\n[C] Enter CONFIG mode";
        fill(255); textSize(9); textAlign(LEFT, TOP);
        text(instr, 10, 10);
        
    }
    
  
  
    /*
    * Flip the frame from webcam to create a mirrored image. Resize the frame
    * if is not in the correct aspect ratio
    * @param img to flip
    * @return mirrored image
    */
    private PImage flipImage( PImage img ) {
        PImage mirrored = new PImage( img.width, img.height );
        for( int i=0; i < img.width; i++ ){
            for(int j=0; j < img.height; j++) mirrored.set( img.width - 1 - i, j, img.get(i, j) );
        }
        if( (float) img.width / (float) img.height != ASPECT_RATIO ) mirrored.resize( int(ASPECT_RATIO * img.height) , img.height);
        return mirrored;
    }
    
  
  
    /*
    * Load camera settings from file
    * @param configFile  File containing all camera parameters
    */
    public void loadCamSettings(File settingsFile) {
        String[] config = loadStrings(settingsFile);
        for(int i=0; i<config.length; i++) {
            String[] c = split(config[i],"  ");
        
            // Camera configuration
            if(c[0].equals("CAMERA")) {
                String[] size = split(c[1]," ");
                camSize = int( split(size[1],",") );
                fRate = int( split(c[2]," ")[1] ); 
          
            // Grids configuration
            } else if( c[0].equals("GRID") ) {
                String[] vertex = split(c[2]," ");
                int[] TL = int( split(vertex[1],",") );
                int[] BR = int( split(vertex[2],",") );
                int[] dimensions = int( split( split(c[3]," ")[1], ",") );
                createGrid(TL[0], TL[1], BR[0], BR[1], dimensions[0], dimensions[1]);
            }
        }
    }
    
  
  
    /*
    * Save camera settings to file. Override previous existing file
    */
    public void saveCamSettings() {
        PrintWriter settings = createWriter("data/" + CONFIG_FILE);
        settings.println("CAMERA  SIZE " + join( str(camSize), ",") + "  FRATE " + fRate);
        for(Grid grid : grids) {
            PVector cornerTL = grid.getCorner(0);
            PVector cornerBR = grid.getCorner(3);
            int[] dimensions = grid.getDimensions();
            String settingsLine = "GRID  " + grid.getName();
                   settingsLine += "  VERTEX " + int(cornerTL.x) + "," + int(cornerTL.y) + " " + int(cornerBR.x) + "," + int(cornerBR.y);
                   settingsLine += "  DIM " + dimensions[0] + "," + dimensions[1];
            settings.println(settingsLine);
        }
        settings.flush();
        settings.close();
        println("CONFIG SAVED");
        mode = Mode.SCAN;
    }
    
    
    
    /*
    * Load table settings (codes and color references) from file
    * @param codePath  Path of file containing all codes parameters
    * @param colorPath  Path of file containing all colors references
    */
    public void loadTableSettings(String codePath, String colorPath) {
        File codeFile = new File(dataPath(codePath));
        File colorFile = new File(dataPath(colorPath));
        codeRef = codeFile.exists() ? loadCodeSettings(codePath) : null;
        legoColors = colorFile.exists() ? loadColorSettings(colorPath) : null;
        
    }  
  
  
  
    /*
    * Load codes settings from file
    * @param settingsPath  Path of file containing all codes parameters
    * @return Map containing all color codes referenced by its ID
    */
    private HashMap<Integer, int[]> loadCodeSettings(String settingsPath) {
        HashMap<Integer,int[]> refCodes = new HashMap<Integer,int[]>();
        Table tiles = loadTable(settingsPath, "header,tsv");
        for(TableRow tile : tiles.rows()) {
            refCodes.put(tile.getInt("ID"), new int[] { tile.getInt("TL"), tile.getInt("TR"), tile.getInt("BL"), tile.getInt("BR") });
        }
        return refCodes;
    }
    
    
    
    /*
    * Load colors settings from file
    * @param settingsPath Path of file containing all colors parameters
    * @return IntList containing all colors referenced by its ID
    */
    private IntList loadColorSettings(String settingsPath) {
        IntList colors = new IntList();
        Table cS = loadTable(settingsPath, "header, tsv");
        for(TableRow c : cS.rows()) colors.append( decodeColor( c.getString("VALUE") ) );
        return colors;
    }
  
    
    
    /*
    * Decode color written in 8-bit hexadecimal format { #AARRGGBB } or rgba { rgba(RR,GG,BB,AA) }
    * @param c  String definition of the color
    * @return int color object, black if color cannot be decoded
    */
    private color decodeColor(String c) {
        // Hex format #AARRGGBB
        if( c.indexOf("#") != -1 ) {  
            return unhex( c.substring(1) );
        // aRGB format rgba(RR, GG, BB, AA)
        } else if( c.indexOf("rgba(") != -1 ) {
            String[] RGBA = split( c.substring(5, c.length()-1) , ",");
            return color( int(RGBA[0]), int(RGBA[1]), int(RGBA[2]), int(RGBA[3]) ); 
        }
        return 0;
    }
    
    
    
    /*
    * Handle mouse events. In CONFIG mode select/unselect grids and enable grid's
    * corners dragging. In COLOR_CONFIG mode select a new color as a reference
    * @param event  Mouse event (PRESS, DRAG, RELEASE)
    */
    public void mouseEvent(MouseEvent event) {
        switch(event.getAction()) {
            case MouseEvent.PRESS:
                switch(mode) {
                    case CONFIG:
                        for(Grid grid : grids) grid.click();
                        break;
                    case CONFIG_COLOR:
                        refColors.set(iColor, get(mouseX, mouseY));
                        break;
                }
                break;
            case MouseEvent.DRAG:
                switch(mode) {
                    case CONFIG:
                        for(Grid grid : grids) grid.drag();
                }
                break;
        }
    }
    
    
    
    /*
    * Handle keyboard events. Allow changing of modes. In CONFIG mode add/remove grids
    * and add/remove columns and rows. In COLOR_CONFIG mode iterate through colors to
    * select its reference
    * @param event  Keyboard event (PRESS, RELEASE, TYPE)
    */
    public void keyEvent(KeyEvent event) {
        switch (event.getAction()) {
            case KeyEvent.PRESS:
                switch(mode) {
                    
                    case CONFIG:
                        switch(event.getKey()) {
                            case 's': case 'S':
                                saveCamSettings();
                                mode = Mode.SCAN;
                                break;
                            case 'r': case 'R':
                                iColor = 0;
                                mode = Mode.CONFIG_COLOR;
                                break;
                            case '+':  // Create new DEFAULT grid --->
                                createGrid(width-190, height-190, width-10, height-10, 5, 5);
                                break;
                            case '-':  // Remove SELECTED grid --->
                                for(int i = 0; i < grids.size(); i++) {
                                    if(grids.get(i).isSelected()) grids.remove(i);
                                }
                                break;
                            case CODED:
                                switch(keyCode) {
                                    case UP:  // ADD row to selected grid --->
                                        for(Grid grid : grids) grid.changeColsRows(0,-1);
                                        break;
                                    case DOWN:  // REMOVE row to selected grid --->
                                        for(Grid grid : grids) grid.changeColsRows(0,1);
                                        break;
                                    case LEFT:  // REMOVE column to selected grid --->
                                        for(Grid grid : grids) grid.changeColsRows(-1,0);
                                        break;
                                    case RIGHT:  // ADD grid to selected grid --->
                                        for(Grid grid : grids) grid.changeColsRows(1,0);
                                        break;
                                }
                                break;
                        }
                        break;
                        
                    case CONFIG_COLOR:
                        switch(event.getKey()) {
                            case ENTER:
                                iColor = (iColor + 1) % refColors.size();
                                break;
                             
                            case 's': case 'S':
                                cursor();
                                mode = Mode.CONFIG;
                                break;
                        }
                        break;
                        
                    case SCAN:
                        switch(event.getKey()) {
                            case 'c':
                                mode = Mode.CONFIG;
                                break;
                        }
                        break;
                }
                break;
        }
    }
  
  
}



// GRID CLASS =========================================================================================================================
public class Grid {
  
    /* ATTRIBUTES ---------------------------------------------------------------------------------> */
    String name;
    PVector TL, BR;
    int[] dimensions;    // Cols & Rows
    int[][] codes;
    boolean selected;
    boolean changed;
  
  
    /* CONSTRUCTORS -------------------------------------------------------------------------------> */
    Grid(int id, int x1, int y1, int x2, int y2, int cols, int rows) {
        name = "GRID"+id;
        TL = new PVector(x1, y1);
        BR = new PVector(x2, y2);
        dimensions = new int[] { cols, rows };
        codes = new int[ dimensions[0] * dimensions[1] ][4];
    }
  
  
    /* GETTERS & SETTERS -------------------------------------------------------------------------> */
    public boolean hasChanged() { return changed; }
    public boolean isSelected() { return selected; }
    public String getName() { return name; }
    public PVector getCorner(int i) { return i == 0 ? TL : BR; }
    public int[] getDimensions() { return dimensions; }
  
  
    /* METHODS -----------------------------------------------------------------------------------> */
  
    /*
    * Change number of columns and rows
    * @param c  Column increment/decrement
    * @param r  Row increment/decrement
    */
    public void changeColsRows(int c, int r) {
        if(selected && dimensions[0] >= -c && dimensions[1] >= -r) {
            dimensions[0] += c;
            dimensions[1] += r;
            codes = new int[ dimensions[0] * dimensions[1] ][4];
        }
    }
    
  
  
    /*
    * Draw grid and all cell's quarters identified colors
    * @param colors  List of identified colors to draw
    */
    public void draw(IntList colors) {
        PVector size = PVector.sub(BR, TL); 
        float tileSize = min( size.x / dimensions[0], size.y / dimensions[1] );
        int qSize = int(tileSize / 2);
        int sqSize = int(qSize * 0.6);
        color linesC = selected ? #00FF00 : #FFFFFF;
        
        pushMatrix();
            translate(TL.x, TL.y);
            noFill(); stroke(linesC, 85); strokeWeight(1);
            rect(0, 0, size.x, size.y);
            for(int y = 0; y < dimensions[1]; y++) {
                for(int x = 0; x < dimensions[0]; x++) {
                    pushMatrix();
                        translate(x * tileSize, y * tileSize);
                        noFill(); stroke(linesC); strokeWeight(1); 
                        rect(0, 0, tileSize, tileSize);
                        // Draw cell's quarters colors
                        int i = y * dimensions[0] + x;
                        for(int j = 0; j < codes[i].length; j++) {
                            fill(colors.get(codes[i][j])); noStroke();
                            rect(((j % 2) + 0.2) * qSize, ((j / 2) + 0.2) * qSize, sqSize, sqSize);
                        }
                    popMatrix();
                }
            }
            // Draw draggable corners
            if(selected) {  
                fill(linesC); noStroke();
                ellipse(0, 0, 10, 10);
                ellipse(size.x, size.y, 10, 10);
            }
        popMatrix();
    }
  
  
  
    /*
    * Evaluate grid. Iterate through all cells and evaluate them saving its code to an array.
    * Cange codes array and set up a flag is something has changed in the array
    * @param cam  Camera image
    * @param refColors  Reference colors to compare detected colors in grid
    */
    public void eval(PImage cam, IntList refColors) {
        changed = false;
        PVector size = PVector.sub(BR, TL);
        float tileSize = min( size.x / dimensions[0], size.y / dimensions[1] );
        for(int y = 0; y < dimensions[1]; y++) {
            for(int x = 0; x < dimensions[0]; x++) {
                PImage tile = cam.get(int(TL.x + x * tileSize), int(TL.y + y * tileSize), int(tileSize), int(tileSize));
                int[] code = evalTile(tile, refColors);
                // Set change flag to true if code has changed 
                int i = y * dimensions[0] + x;
                for(int j = 0; j < code.length; j++) {
                    if(code[j] != codes[i][j]) {
                        changed = true;
                        codes[i][j] = code[j];
                    }
                }
            }
        }
    }
    
    
    
    /*
    * Evaluate cell. Divide it in quarters and calculate the average color for each one, comparing
    * to reference colors to find closer one
    * @param imgCell  Cell image cropped from camera frame
    * @param refColors  Reference colors to compare detected colors in cell's quarters
    */
    public int[] evalTile(PImage imgCell, IntList refColors) {
        int qSize = imgCell.width / 2;
        int evalSize = int(qSize * 0.6);
        int[] code = new int[4];
        for(int i = 0; i < 4; i++) {
            PImage qCell = imgCell.get(int(((i % 2) + 0.2) * qSize), int(((i / 2) + 0.2) * qSize), evalSize, evalSize);
            code[i] = compareColor(getAvgColor(qCell), refColors);
        }
        return code;
    }
    
    
    
    /*
    * Prepare grid message, providing all cells configuration (ID, position and rotation)
    * @param refCodes  List of reference codes and their IDs
    * @return String message with grid layour configuration
    */
    public String getMsg(HashMap<Integer, int[]> refCodes) {
        String[] msgLines = new String[1 + dimensions[0] * dimensions[1]];
        msgLines[0] = dimensions[0] + "  " + dimensions[1];
        for(int i = 0; i < codes.length; i++ ) {
            int id = -1;
            for(Map.Entry code : refCodes.entrySet()){
                if( arrayMatch(codes[i], (int[]) code.getValue()) ) {
                    id = (int) code.getKey();
                    break;
                }
          }
          msgLines[i+1] = id + "  " + (i % dimensions[0]) + "  " + (i / dimensions[1]) + "  rotation"; 
        }
        return join(msgLines, "\n");
    }



    /*
    * Mouse click listener. Select or unselect if mouse click happened over the grid
    */
    public void click() {
        selected = mouseOverGrid() || mouseOverPoint(TL, 20) || mouseOverPoint(BR, 20);
    }
  
  
  
    /*
    * Mouse drag listener. Drags TopLeft or BottomRight corners if grid is selected
    */
    public void drag() {
        if(selected) {
            println("MOUSE DRAGGED");
            if( mouseOverPoint(TL, 15) ) TL = new PVector(mouseX, mouseY);
            if( mouseOverPoint(BR, 15) ) BR = new PVector(mouseX, mouseY);
        }
    }
  
  
    /*
    * Check if mouse is over the grid
    * @return true if mouse is over the grid, false if not
    */
    public boolean mouseOverGrid() {
        return mouseX > TL.x && mouseX < BR.x && mouseY > TL.y && mouseY < BR.y;
    }
  
  
    /*
    * Check if mouse is over determined point within a margin
    * @return true if mouse is over the point, false if not
    */
    public boolean mouseOverPoint( PVector point, int margin ) {
        if( dist(point.x, point.y, mouseX, mouseY) < margin ) return true;
        else return false;
    }
  
  
  
    /*
    * Check if two arrays values match
    * @param a1  First array to compare
    * @param a2  Second array to compare
    * @return true if all values in both arrays match, false if not
    */
    private boolean arrayMatch(int[] a1, int[] a2) {
        if(a1.length != a2.length) return false;
        else {
            for(int i = 0; i < a1.length; i++) {
                if(a1[i] != a2[i]) return false;
            }
        }
        return true;
    }
   
}




/*
* Find average color in image
* @param img  Image to find average color
* @return average color
*/
public color getAvgColor(PImage img) {
    img.loadPixels();
    int R = 0, G = 0, B = 0;
    for (int i=0; i<img.pixels.length; i++) {
        R += img.pixels[i] >> 16&0xFF;
        G += img.pixels[i] >> 8&0xFF;
        B += img.pixels[i] &0xFF;
    }
    R /= img.pixels.length;
    G /= img.pixels.length;
    B /= img.pixels.length;
    return color(R, G, B); 
}



/*
* Comapre a color with those inside a reference array
* @param c  Color to compare
* @param refColors  Colors list with whom compare color
* @return code of the closes found color. -1 means not color found
*/
public int compareColor(color c, IntList refColors) {
    float minDist = Float.MAX_VALUE;
    int codeColor = -1;
    for(int i = 0; i < refColors.size(); i++) {
        float Rmean =(red(refColors.get(i)) + red(c)) / 2;
        float R = red(refColors.get(i)) - red(c);
        float G = green(refColors.get(i)) - green(c);
        float B = blue(refColors.get(i)) - blue(c);
        float dist = sqrt((int(((512+Rmean)*R*R))>>8)+(4*G*G)+(int(((767-Rmean)*B*B))>>8));
        if( dist < minDist ) {
            minDist = dist;
            codeColor = i;
        }
    }
    return codeColor;
}